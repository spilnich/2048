package com.javarush.task.task35.task3513;


import java.util.*;

public class Model {
    private static final int FIELD_WIDTH = 4;
    private Tile[][] gameTiles = new Tile[FIELD_WIDTH][FIELD_WIDTH];
    int score;
    int maxTile;

    private Stack<Tile[][]> previousStates = new Stack<>();
    private Stack<Integer> previousScores = new Stack<>();
    private boolean isSaveNeeded = true;

    public Model() {
        resetGameTiles();
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }

    private List<Tile> getEmptyTiles() {
        List<Tile> list = new ArrayList<>();

        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                if (gameTiles[i][j].value == 0) {
                    list.add(gameTiles[i][j]);
                }
            }
        }
        return list;
    }

    private void addTile() {
        if (getEmptyTiles().isEmpty()) {
        } else {
            int idOfRandomTile = (int) (Math.random() * getEmptyTiles().size());
            getEmptyTiles().get(idOfRandomTile).value = Math.random() < 0.9 ? 2 : 4;
        }
    }

    void resetGameTiles() {
        score = 0;
        maxTile = 0;
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                gameTiles[i][j] = new Tile();
            }
        }
        addTile();
        addTile();
    }

    private boolean compressTiles(Tile[] tiles) {
        Tile[] copy = tiles.clone();
        Arrays.sort(tiles, Comparator.comparing(v -> v.value == 0));
        return !Arrays.equals(copy, tiles);
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean isChanged = false;
        for (int i = 0; i < tiles.length; i++) {
            if (i + 1 < tiles.length) {
                if (tiles[i].value == tiles[i + 1].value) {
                    if (tiles[i].value != 0 && tiles[i + 1].value != 0) {
                        tiles[i].value *= 2;
                        tiles[i + 1].value = 0;
                        score += tiles[i].value;
                        if (tiles[i].value > maxTile) {
                            maxTile = tiles[i].value;
                        }
                        isChanged = true;
                    }
                }
            }
            compressTiles(tiles);
        }
        return isChanged;
    }

    public void left() {
        if (isSaveNeeded) {
            saveState(gameTiles);
        }
        for (Tile[] gameTile : gameTiles) {
            if (compressTiles(gameTile) | mergeTiles(gameTile)) {
                addTile();
            }
        }
        isSaveNeeded = true;
    }


    public void up() {
        saveState(gameTiles);
        getTurnedArray();
        getTurnedArray();
        getTurnedArray();
        left();
        getTurnedArray();
    }

    public void right() {
        saveState(gameTiles);
        getTurnedArray();
        getTurnedArray();
        left();
        getTurnedArray();
        getTurnedArray();
    }

    public void down() {
        saveState(gameTiles);
        getTurnedArray();
        left();
        getTurnedArray();
        getTurnedArray();
        getTurnedArray();
    }

    public void getTurnedArray() {
        Tile[][] temp = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                temp[i][j] = gameTiles[FIELD_WIDTH - j - 1][i];
            }
        }
        gameTiles = temp;
    }

    public boolean canMove() {
        boolean canMove = false;
        if (!getEmptyTiles().isEmpty()) {
            canMove = true;
        }
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                if (j + 1 < FIELD_WIDTH && i + 1 < FIELD_WIDTH) {
                    if (gameTiles[i][j].value == gameTiles[i][j + 1].value ||
                            gameTiles[i][j].value == gameTiles[i + 1][j].value) {
                        canMove = true;
                    }
                }
            }
        }
        return canMove;
    }

    private void saveState(Tile[][] tiles) {
        Tile[][] temp = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                temp[i][j] = new Tile(tiles[i][j].value);
            }
        }
        previousStates.push(temp);
        previousScores.push(score);
        isSaveNeeded = false;
    }

    public void rollback() {
        if (!previousScores.empty()) {
            score = previousScores.pop();
        }
        if (!previousStates.empty()) {
            gameTiles = previousStates.pop();
        }
    }

    public void randomMove() {
        int n = ((int) (Math.random() * 100)) % 4;
        switch (n) {
            case 0:
                left();
                break;
            case 1:
                right();
                break;
            case 2:
                up();
                break;
            case 3:
                down();
                break;
        }
    }

    public boolean hasBoardChanged() {
        int summaryTilesWeight = 0;
        int summarySavedTilesWeight = 0;
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                summaryTilesWeight += gameTiles[i][j].value;
            }
        }
        for (int i = 0; i < previousStates.peek().length; i++) {
            for (int j = 0; j < previousStates.peek().length; j++) {
                summarySavedTilesWeight += previousStates.peek()[i][j].value;
            }
        }
        return summaryTilesWeight != summarySavedTilesWeight;
    }

    public MoveEfficiency getMoveEfficiency(Move move) {
        move.move();
        if (!hasBoardChanged()) {
            return new MoveEfficiency(-1, 0, move);
        } else {
            rollback();
            return new MoveEfficiency(getEmptyTiles().size(), score, move);
        }
    }

    public void autoMove() {
        PriorityQueue<MoveEfficiency> queue = new PriorityQueue<>(4, Collections.reverseOrder());
            queue.offer(new MoveEfficiency(getEmptyTiles().size(), score, new Move() {  // FOR EXAMPLE
                @Override
                public void move() {
                    left();
                }
            }));                                                                         // SHORTER:
            queue.offer(new MoveEfficiency(getEmptyTiles().size(), score, this::right));
            queue.offer(new MoveEfficiency(getEmptyTiles().size(), score, this::up));
            queue.offer(new MoveEfficiency(getEmptyTiles().size(), score, this::down));

            queue.peek().getMove().move();
    }
}